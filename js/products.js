function addProduct(){

    let productName = document.getElementById("productName").value;
    let description = document.getElementById("description").value;
    let price = document.getElementById("price").value;
    let type = document.getElementById("type").value;

    let product = {
        "ProductName": productName,
        "Description": description,
        "Price": price,
        "Type": type
    }

    fetch('http://localhost:32223/product', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(product),
    })
    .then(response => response.json())
    .then(data => {
    console.log('Success:', data);
    })
    .catch((error) => {
    console.error('Error:', error);
    });

}